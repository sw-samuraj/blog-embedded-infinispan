# Embedded Infinispan #

An example project for demonstration of [Infinispan](http://infinispan.org/) used
as an embedded data grid in the form of the *TreeCache* with
the usage of the cache listeners.

## How to run the example

1. Clone the repository.
1. Run the command `gradle tomcatRun`.
1. Open *SoapUI* project from `src/test/soapui`.
1. Run the test suite *Cases + Persons*.
1. Check the log in the console.
1. Open a browser on URL <http://localhost:4040/deep-thought> to see date read from the cache.
1. **Browse the source code.**

## What if you're sitting behind a proxy?

You need to modify (or create) your `$HOME/.gradle/gradle.properties` in
following way:

```
#!properties
systemProp.http.proxyHost=<your-proxy-host>
systemProp.http.proxyPort=<your-proxy-port>
systemProp.http.nonProxyHosts=localhost|<other-non-proxy-host>
systemProp.https.proxyHost=<your-proxy-host>
systemProp.https.proxyPort=<your-proxy-port>
systemProp.https.nonProxyHosts=localhost|<other-non-proxy-host>
```

## License ##

The **blog-embedded-infinispan** project is published under [BSD 3-Clause](http://opensource.org/licenses/BSD-3-Clause) license.
