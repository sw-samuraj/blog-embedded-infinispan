package cz.swsamuraj.infinispan.document.cache;

import cz.swsamuraj.infinispan.document.logic.DocumentService;
import cz.swsamuraj.infinispan.ws.model.DocumentInfo;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CacheDocumentService implements DocumentService {

    @Autowired
    private Mapper mapper;

    @Override
    public Long storeDocument(DocumentInfo document) {
        DocumentNode documentNode = mapper.map(document, DocumentNode.class);

        return null;
    }

}
