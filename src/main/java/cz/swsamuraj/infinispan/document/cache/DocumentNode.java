package cz.swsamuraj.infinispan.document.cache;

import cz.swsamuraj.infinispan.ws.model.DocumentType;

public class DocumentNode {

    private String personID;

    private String documentID;

    private DocumentType documentType;

    public String getPersonID() {
        return personID;
    }

    public void setPersonID(String personID) {
        this.personID = personID;
    }

    public String getDocumentID() {
        return documentID;
    }

    public void setDocumentID(String documentID) {
        this.documentID = documentID;
    }

    public DocumentType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DocumentType documentType) {
        this.documentType = documentType;
    }

}
