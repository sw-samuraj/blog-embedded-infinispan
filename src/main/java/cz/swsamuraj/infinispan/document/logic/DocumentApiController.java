package cz.swsamuraj.infinispan.document.logic;

import cz.swsamuraj.infinispan.ws.api.DocumentApi;
import cz.swsamuraj.infinispan.ws.model.DocumentInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class DocumentApiController implements DocumentApi {

    private static final Logger logger = LogManager.getLogger(DocumentApiController.class);

    @Autowired
    private DocumentService documentService;

    @Override
    public ResponseEntity<Void> putDocument(@RequestBody DocumentInfo document) {
        logger.debug("DocumentInfo:\n" + document);

        Long id = documentService.storeDocument(document);

        logger.debug("DocumentNode[ID: {}]", id);

        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }

}
