package cz.swsamuraj.infinispan.document.logic;

import cz.swsamuraj.infinispan.ws.model.DocumentInfo;

public interface DocumentService {

    Long storeDocument(DocumentInfo document);

}
