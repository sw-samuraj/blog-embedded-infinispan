package cz.swsamuraj.infinispan;

import cz.swsamuraj.infinispan.caze.cache.CaseKeyFilter;
import cz.swsamuraj.infinispan.caze.cache.CaseObserver;
import cz.swsamuraj.infinispan.person.cache.PersonKeyFilter;
import cz.swsamuraj.infinispan.person.cache.PersonObserver;
import org.dozer.DozerBeanMapper;
import org.infinispan.Cache;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.tree.TreeCache;
import org.infinispan.tree.TreeCacheFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.mvc.annotation.AnnotationMethodHandlerAdapter;

import java.util.Arrays;
import java.util.List;

@Configuration
@ComponentScan("cz.swsamuraj.infinispan")
public class SpringConfiguration {

    @Bean(destroyMethod = "stop")
    public DefaultCacheManager defaultCacheManager() {
        org.infinispan.configuration.cache.Configuration configuration =
                new ConfigurationBuilder()
                        .invocationBatching().enable()
                        .build();

        return new DefaultCacheManager(configuration);
    }

    @Bean
    public TreeCache<String, Object> treeCache(DefaultCacheManager cacheManager) {
        Cache<String, Object> cache = cacheManager.getCache();
        cache.addListener(new CaseObserver(), new CaseKeyFilter());
        cache.addListener(new PersonObserver(), new PersonKeyFilter());

        return new TreeCacheFactory().createTreeCache(cache);
    }

    @Bean
    public DozerBeanMapper dozerMapper() {
        List<String> mappingFiles = Arrays.asList("dozer-bean-mappings.xml");
        DozerBeanMapper dozerBean = new DozerBeanMapper();
        dozerBean.setMappingFiles(mappingFiles);

        return dozerBean;
    }

    @Bean
    public AnnotationMethodHandlerAdapter annotationMethodHandlerAdapter() {

        AnnotationMethodHandlerAdapter adapter = new AnnotationMethodHandlerAdapter();
        HttpMessageConverter<?>[] converters = {new MappingJackson2HttpMessageConverter()};
        adapter.setMessageConverters(converters);

        return adapter;
    }

}
