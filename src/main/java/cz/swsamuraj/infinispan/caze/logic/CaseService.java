package cz.swsamuraj.infinispan.caze.logic;

import cz.swsamuraj.infinispan.caze.cache.CaseNode;
import cz.swsamuraj.infinispan.ws.model.CaseInfo;

import java.util.List;

public interface CaseService {

    void storeCase(CaseInfo caseInfo);

    List<CaseNode> retrieveAllCases();

    CaseNode retrieveCase(String caseID);
}
