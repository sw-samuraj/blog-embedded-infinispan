package cz.swsamuraj.infinispan.caze.cache;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.infinispan.notifications.Listener;
import org.infinispan.notifications.cachelistener.annotation.CacheEntryCreated;
import org.infinispan.notifications.cachelistener.annotation.CacheEntryModified;
import org.infinispan.notifications.cachelistener.event.CacheEntryCreatedEvent;
import org.infinispan.notifications.cachelistener.event.CacheEntryEvent;
import org.infinispan.notifications.cachelistener.event.CacheEntryModifiedEvent;

@Listener
public class CaseObserver {

    private static final Logger logger = LogManager.getLogger(CaseObserver.class);

    @CacheEntryCreated
    public void cacheEntryCreated(CacheEntryCreatedEvent event) {
        fireWebSocketEvent("CacheEntryCreated", event);
    }

    @CacheEntryModified
    public void cacheEntryModified(CacheEntryModifiedEvent event) {
        fireWebSocketEvent("CacheEntryModified", event);
    }

    private void fireWebSocketEvent(String eventType, CacheEntryEvent event) {
        logger.debug("{} has been triggered: {}", eventType, event.getKey());
    }

}
