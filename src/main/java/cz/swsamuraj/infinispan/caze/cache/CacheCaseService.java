package cz.swsamuraj.infinispan.caze.cache;

import cz.swsamuraj.infinispan.caze.logic.CaseService;
import cz.swsamuraj.infinispan.ws.model.CaseInfo;
import org.dozer.Mapper;
import org.infinispan.tree.Fqn;
import org.infinispan.tree.Node;
import org.infinispan.tree.TreeCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class CacheCaseService implements CaseService {

    @Autowired
    private Mapper mapper;

    @Autowired
    private TreeCache<String, Object> cache;

    @Override
    public void storeCase(CaseInfo caseInfo) {
        CaseNode caseNode = mapper.map(caseInfo, CaseNode.class);
        String caseID = caseInfo.getCaseID();
        Fqn fqn = getFqn(caseID);

        cache.put(fqn, caseID, caseNode);
    }

    @Override
    public List<CaseNode> retrieveAllCases() {
        Node<String, Object> caseNode = cache.getNode("/case");

        if (caseNode != null) {
            Set<Node<String, Object>> children = caseNode.getChildren();

            return children.stream()
                    .map(node -> (CaseNode) node.get(node.getKeys().iterator().next()))
                    .collect(Collectors.toList());
        } else {
            return Collections.<CaseNode>emptyList();
        }

    }

    @Override
    public CaseNode retrieveCase(String caseID) {
        Fqn fqn = getFqn(caseID);

        return (CaseNode) cache.get(fqn, caseID);
    }

    private Fqn getFqn(String caseID) {
        return Fqn.fromElements("case", caseID);
    }

}
