package cz.swsamuraj.infinispan.caze.cache;

import org.infinispan.filter.KeyFilter;
import org.infinispan.tree.impl.NodeKey;

public class CaseKeyFilter implements KeyFilter {

    @Override
    public boolean accept(Object key) {
        NodeKey nodeKey = (NodeKey) key;

        if (nodeKey.getFqn().isRoot()
                || nodeKey.getFqn().size() != 2
                || nodeKey.getContents().equals(NodeKey.Type.STRUCTURE)) {
            return false;
        }

        return nodeKey.getFqn().hasElement("case");
    }
}
