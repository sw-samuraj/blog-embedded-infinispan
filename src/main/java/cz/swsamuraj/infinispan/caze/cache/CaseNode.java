package cz.swsamuraj.infinispan.caze.cache;

import java.time.OffsetDateTime;

public class CaseNode {

    private String caseID;

    private OffsetDateTime timestamp;

    public String getCaseID() {
        return caseID;
    }

    public void setCaseID(String caseID) {
        this.caseID = caseID;
    }

    public OffsetDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(OffsetDateTime timestamp) {
        this.timestamp = timestamp;
    }

}
