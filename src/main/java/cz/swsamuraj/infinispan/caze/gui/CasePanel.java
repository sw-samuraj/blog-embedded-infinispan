package cz.swsamuraj.infinispan.caze.gui;

import cz.swsamuraj.infinispan.caze.cache.CaseNode;
import cz.swsamuraj.infinispan.caze.logic.CaseService;
import cz.swsamuraj.infinispan.person.gui.PersonPanel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.wicket.ajax.AjaxEventBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.event.IEvent;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.protocol.ws.WebSocketSettings;
import org.apache.wicket.protocol.ws.api.WebSocketBehavior;
import org.apache.wicket.protocol.ws.api.WebSocketPushBroadcaster;
import org.apache.wicket.protocol.ws.api.event.WebSocketPushPayload;
import org.apache.wicket.protocol.ws.api.message.ConnectedMessage;
import org.apache.wicket.protocol.ws.api.message.IWebSocketPushMessage;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class CasePanel extends Panel implements Observer {

    private static final Logger logger = LogManager.getLogger(CasePanel.class);

    @SpringBean
    private CaseService caseService;

    private Label currentCase;
    private WebMarkupContainer wrapper;

    public CasePanel(String id) {
        super(id);

        wrapper = new WebMarkupContainer("wrapper");
        wrapper.setOutputMarkupId(true);
        wrapper.add(getCasesListView());
        add(wrapper);

        currentCase = new Label("currentCase");
        currentCase.setOutputMarkupId(true);
        add(currentCase);

        add(new PersonPanel("personPanel42", "42"));
        add(new PersonPanel("personPanel12", "12"));

        add(new WebSocketBehavior() {
            @Override
            protected void onConnect(ConnectedMessage message) {
                logger.debug("WebSocket client connected.");
            }
        });
    }

    private ListView<CaseNode> getCasesListView() {
        return new ListView<CaseNode>("cases", casesModel()) {
            @Override
            protected void populateItem(ListItem<CaseNode> item) {
                item.add(new Label("caseID", new PropertyModel<String>(item.getModel(), "caseID")));
                item.add(new Label("timestamp", new PropertyModel<String>(item.getModel(), "timestamp")));
                item.add(new AjaxEventBehavior("click") {
                    @Override
                    protected void onEvent(AjaxRequestTarget target) {
                        CaseNode caseNode = item.getModelObject();
                        currentCase.setDefaultModel(Model.of(caseNode.getCaseID()));

                        target.add(currentCase);
                    }
                });
            }
        };
    }

    private IModel<List<CaseNode>> casesModel() {
        return new LoadableDetachableModel<List<CaseNode>>() {
            @Override
            protected List<CaseNode> load() {
                return caseService.retrieveAllCases();
            }
        };
    }

    @Override
    public void onEvent(IEvent<?> event) {
        if (event.getPayload() instanceof WebSocketPushPayload) {
            WebSocketPushPayload wsEvent = (WebSocketPushPayload) event.getPayload();
            IWebSocketPushMessage message = wsEvent.getMessage();

            if (message instanceof CaseEvent) {
                wsEvent.getHandler().add(wrapper);
            }
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof CaseEvent) {
            CaseEvent event = (CaseEvent) arg;

            logger.debug("Observable has sent: {}", event);

            WebSocketSettings webSocketSettings = WebSocketSettings.Holder.get(getApplication());
            WebSocketPushBroadcaster broadcaster = new WebSocketPushBroadcaster(webSocketSettings.getConnectionRegistry());
            broadcaster.broadcastAll(getApplication(), event);
        }
    }
}
