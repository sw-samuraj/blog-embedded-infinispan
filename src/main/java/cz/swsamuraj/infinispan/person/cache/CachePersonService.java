package cz.swsamuraj.infinispan.person.cache;

import cz.swsamuraj.infinispan.person.logic.PersonService;
import cz.swsamuraj.infinispan.ws.model.PersonInfo;
import org.dozer.Mapper;
import org.infinispan.tree.Fqn;
import org.infinispan.tree.TreeCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CachePersonService implements PersonService {

    @Autowired
    private Mapper mapper;

    @Autowired
    private TreeCache<String, Object> cache;

    @Override
    public void storePerson(PersonInfo personInfo) {
        PersonNode personNode = mapper.map(personInfo, PersonNode.class);
        String caseID = personInfo.getCaseID();
        String personID = personInfo.getPersonID();
        Fqn fqn = getFqn(caseID, personID);

        cache.put(fqn, personID, personNode);
    }

    @Override
    public PersonNode retrievePerson(String caseID, String personID) {
        Fqn fqn = getFqn(caseID, personID);

        return (PersonNode) cache.get(fqn, personID);
    }

    private Fqn getFqn(String caseID, String personID) {
        return Fqn.fromElements("case", caseID, "person", personID);
    }

}
