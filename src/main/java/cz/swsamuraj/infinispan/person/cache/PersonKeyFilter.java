package cz.swsamuraj.infinispan.person.cache;

import org.infinispan.filter.KeyFilter;
import org.infinispan.tree.impl.NodeKey;

public class PersonKeyFilter implements KeyFilter {

    @Override
    public boolean accept(Object key) {
        NodeKey nodeKey = (NodeKey) key;

        if (nodeKey.getFqn().isRoot()
                || nodeKey.getFqn().size() != 4
                || nodeKey.getContents().equals(NodeKey.Type.STRUCTURE)) {
            return false;
        }

        return nodeKey.getFqn().hasElement("person");
    }
}
