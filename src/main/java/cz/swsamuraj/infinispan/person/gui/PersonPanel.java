package cz.swsamuraj.infinispan.person.gui;

import cz.swsamuraj.infinispan.person.cache.PersonNode;
import cz.swsamuraj.infinispan.person.logic.PersonService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.wicket.event.IEvent;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.protocol.ws.WebSocketSettings;
import org.apache.wicket.protocol.ws.api.WebSocketBehavior;
import org.apache.wicket.protocol.ws.api.WebSocketPushBroadcaster;
import org.apache.wicket.protocol.ws.api.event.WebSocketPushPayload;
import org.apache.wicket.protocol.ws.api.message.IWebSocketPushMessage;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.Observable;
import java.util.Observer;

public class PersonPanel extends Panel implements Observer {

    private static final Logger logger = LogManager.getLogger(PersonPanel.class);

    @SpringBean
    private PersonService personService;

    private String caseID = "987";
    private String personID;
    private WebMarkupContainer wrapper;

    public PersonPanel(String id, String personID) {
        super(id);

        this.personID = personID;

        add(new Label("personIDHeader", "Person #" + personID));

        setDefaultModel(new CompoundPropertyModel<>(getModel()));

        wrapper = new WebMarkupContainer("wrapper");
        wrapper.setOutputMarkupId(true);
        add(wrapper);

        wrapper.add(new Label("personID"));
        wrapper.add(new Label("firstName"));
        wrapper.add(new Label("lastName"));
        wrapper.add(new Label("sex"));
        wrapper.add(new Label("timestamp"));

        add(new WebSocketBehavior() {});
    }

    private IModel<PersonNode> getModel() {
        return new LoadableDetachableModel<PersonNode>() {
            @Override
            protected PersonNode load() {
                PersonNode personNode = personService.retrievePerson(caseID, personID);

                if (personNode != null) {
                    return personNode;
                } else {
                    return new PersonNode();
                }
            }
        };
    }

    @Override
    public void onEvent(IEvent<?> event) {
        if (event.getPayload() instanceof WebSocketPushPayload) {
            WebSocketPushPayload wsEvent = (WebSocketPushPayload) event.getPayload();
            IWebSocketPushMessage message = wsEvent.getMessage();

            if (message instanceof PersonEvent) {
                PersonEvent personMessage = (PersonEvent) message;

                if (personID.equals(personMessage.getPersonID())) {
                    wsEvent.getHandler().add(wrapper);
                }
            }
        }
    }

    @Override
    public void update(Observable observable, Object arg) {
        if (arg instanceof PersonEvent) {
            PersonEvent event = (PersonEvent) arg;

            logger.debug("Observable has sent: {}", event);

            WebSocketSettings webSocketSettings = WebSocketSettings.Holder.get(getApplication());
            WebSocketPushBroadcaster broadcaster = new WebSocketPushBroadcaster(webSocketSettings.getConnectionRegistry());
            broadcaster.broadcastAll(getApplication(), event);
        }
    }

}
