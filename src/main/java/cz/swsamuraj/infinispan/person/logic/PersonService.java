package cz.swsamuraj.infinispan.person.logic;

import cz.swsamuraj.infinispan.person.cache.PersonNode;
import cz.swsamuraj.infinispan.ws.model.PersonInfo;

public interface PersonService {

    void storePerson(PersonInfo person);

    PersonNode retrievePerson(String caseID, String personID);
}
