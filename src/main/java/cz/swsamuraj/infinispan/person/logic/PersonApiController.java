package cz.swsamuraj.infinispan.person.logic;

import cz.swsamuraj.infinispan.ws.api.PersonApi;
import cz.swsamuraj.infinispan.ws.model.PersonInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class PersonApiController implements PersonApi {

    private static final Logger logger = LogManager.getLogger(PersonApiController.class);

    @Autowired
    private PersonService personService;

    @Override
    public ResponseEntity<Void> putPerson(@RequestBody PersonInfo person) {
        logger.debug("PersonInfo:\n" + person);

        personService.storePerson(person);

        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }
}
