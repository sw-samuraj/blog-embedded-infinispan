package cz.swsamuraj.infinispan;

import org.apache.wicket.protocol.ws.javax.JavaxWebSocketFilter;

import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;

@WebFilter(value = "/*",
           initParams = {
                   @WebInitParam(name = "applicationFactoryClassName",
                                 value = "org.apache.wicket.spring.SpringWebApplicationFactory")
           })
public class WicketAppFilter extends JavaxWebSocketFilter {
}
