package cz.swsamuraj.infinispan;

import cz.swsamuraj.infinispan.caze.gui.CasePanel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;

public class HomePage extends WebPage {

    private static final Logger logger = LogManager.getLogger(HomePage.class);

    public HomePage() {
        add(new Label("helloLabel", "Hello, Wicket & Spring & Infinispan!"));
        add(new CasePanel("casePanel"));
    }

}
